import React, { useState,useEffect } from "react";
import plus from './images/plus.svg';

const CreateNote = (props) => {

    const [expend,setExpend] = useState(false);
    const [addBtn,setAddBtn] = useState(false);

    let [note,setNote] = useState({
        id:0,
        title: '',
        content: '',
    })

    const inputEvent = (event) => {
        const {name,value} = event.target;
            setNote((oldVal) => {
                return {
                    ...oldVal,
                    [name]: value
                }
            })
    setAddBtn(true)
    }

    const addEvent = (e) => {
        let Id = Math.floor(Math.random() * 1000); 
        const obj = {
            id: Id,
            title:note.title,
            content:note.content,
         }
        
        props.passNote(obj)
        setNote({
            id:'',
            title: '',
            content: '',
        })
        setAddBtn(false)

    }

    const expendIt = () => {
        setExpend(true);
    }

    const hideBtn = () => {
        setExpend(false);
    }
    
    return(<>
        <div className="main_note" onDoubleClick={hideBtn}>
            <form>
                { expend?<input type="text" className="" placeholder="Title" name="title" autoComplete="off" value={note?.title || ''} onChange={inputEvent}/>:null}
                <textarea rows="" cols="" placeholder="Write a note..." name="content" value={note?.content || ''} onChange={inputEvent} onClick={expendIt} ></textarea>
                <div style={{ display:'flex' }}>
                {addBtn?<button type="button" className="plus_sign" onClick={addEvent}>&#x2b;</button>:null}
               
                </div>
            </form>
        </div>
    </>)
}

export default CreateNote;