import "./index.css";
import './App.css';
import Header from './Header';
import Footer from './Footer';
import CreateNote from './CreateNote';
import Note from './Note';
import EditNote from "./EditNote";
import { useState,useEffect } from "react";

 // get data from localstorage
 const getItem = () => {
  let list = localStorage.getItem('notes');
  if(list){
   return JSON.parse(localStorage.getItem('notes'))
  }else{
    return [];
  }
}

function App() {
  const [items,setItems] = useState(getItem());
  const [editItems,setEditItems] = useState(false);
  const [editItemsVal,setEditItemsVal] = useState([]);

  const addNote = (note) => {
    if(note && editItems){
      let arr = items;
      const index = items.findIndex(o=>o.id === note.id)
      arr[index]=note;
      setItems(arr)
      setEditItems(false)

    }else{
    setItems([...items,note])
    }
  }

  const editNote = (id) => {
    if(id){
      let obj =  items.filter((currVal) => {
          return currVal.id === id; 
        })
        setEditItemsVal(obj[0])
        setEditItems(true)
    
    }else{
      setEditItems(false)
    
    }
  }


  const deleteNote = (id) => {
    setItems((oldVal) =>{
      return oldVal.filter((currVal) => {
        return currVal.id !== id;
      })
    })
  }

     // set date into localstorage
  useEffect(() => {
    localStorage.setItem('notes',JSON.stringify(items));
  })

  

  return (
   <>
       <Header/>
       {editItems?<EditNote id={editItemsVal.id} title={editItemsVal.title} content={editItemsVal.content} passNote={addNote}/>:<CreateNote passNote={addNote}/>}
       {items.map((item) => {
         return (<Note key={item.id} id={item.id} title={item.title} content={item.content} deleteItem={deleteNote} editItem={editNote}/>)
       })}
       <Footer/> 
   </>
  );
}

export default App;
