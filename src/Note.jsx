import React from "react";
import {useState} from 'react';
import CreateNote from './CreateNote';

const Note = (props) => {

    const editNote = (id) => {
        props.editItem(id)
    
    }

    const delNote = () => {
        props.deleteItem(props.id);
    }
    return (<>
        <div className="note">
            <h1>{props.title}</h1>
            <br/>
            <p>{props.content}</p>
            <button className="btn" onClick={delNote}><i className="fa fa-times deleteIcon"></i></button>
            <button className="btn" onClick={() => {editNote(props.id)}}><i className="fa fa-edit deleteIcon"></i></button>
        </div>
      
    </>)
}

export default Note;