import React from "react";
import logo from "./images/logo.png"
const Header = () => {
    return(<>
        <div className="header">
        <img src={logo} alt="logo" style={{height:'70px',borderRadius:'50%'}}/>
            <h1>Notes</h1>
        </div>
    </>)
}
export default Header;