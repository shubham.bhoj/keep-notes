import React, { useState,useEffect } from "react";
import plus from './images/plus.svg';

const EditNote = (props) => {

    let [note,setNote] = useState({
        id: props.id,
        title: props.title,
        content: props.content,
    })

    const inputEvent = (event) => {
        const {name,value} = event.target;
            setNote((oldVal) => {
                return {
                    ...oldVal,
                    [name]: value
                }
            })
    // setAddBtn(true)
    }

    const editEvent = () => {
        let obj = {
            id:note.id,
            title: note.title,
            content: note.content,
        }
        props.passNote(obj)
        setNote({
            id:'',
            title: '',
            content: '',
        })
        // setAddBtn(false)

    }


    
    return(<>
        <div className="main_note" >
            <form>
                <input type="text" className="" placeholder="Update Title" name="title" autoComplete="off" value={note?.title || ''} onChange={inputEvent}  />
                <textarea rows="" cols="" placeholder="Update note..." name="content" value={note?.content || ''} onChange={inputEvent} ></textarea>
                <div style={{ display:'flex' }}>
                <button type="button" className="plus_sign" onClick={()=>{editEvent(props.id)}}>&#9998;</button>
                </div>
            </form>
        </div>
    </>)
}

export default EditNote;